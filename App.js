import React from "react";
import { StyleSheet, Text, View, TouchableOpacity, FlatList } from "react-native";
import Icon from 'react-native-vector-icons/MaterialIcons';
import AIcon from 'react-native-vector-icons/AntDesign';

const Flatlist = () => {
  const [data, setdata] = React.useState([
    {
      "_id": "5f290c35eda66c07100c756e",
      "category_name": "All",
      "category_no": 1,
      "status": 1,
      "selectable": 1,
      "order": 1
    },
    {
      "_id": "5f3b9d40d9b75524dcc46587",
      "category_name": "Favourites",
      "category_no": 12,
      "status": 1,
      "selectable": 0,
      "order": 2
    },
    {
      "_id": "5f28e17b6a682e26f4377e75",
      "category_name": "Folktales",
      "category_no": 4,
      "status": 1,
      "order": 4,
      "selectable": 0
    },
    {
      "_id": "605189c77770a524f4a9a58e",
      "category_name": "Mystery",
      "category_no": 19,
      "status": 1,
      "selectable": 0,
      "order": 19
    },
    {
      "_id": "5f28e1666a682e26f4377e74",
      "category_name": "Bedtime Stories",
      "category_no": 3,
      "status": 1,
      "order": 3,
      "selectable": 0
    },
    {
      "_id": "5f28e1eb6a682e26f4377e79",
      "category_name": "General Knowledge",
      "category_no": 8,
      "status": 1,
      "order": 8,
      "selectable": 0
    },
    {
      "_id": "5f28e12f8bbd8326f46ab7cf",
      "category_name": "Science-Fiction",
      "category_no": 2,
      "status": 1,
      "selectable": 0,
      "order": 12
    },
    {
      "_id": "61cbe44f9435dbf04a1c1d81",
      "category_name": "Action",
      "category_no": 23,
      "status": 1,
      "selectable": 0,
      "order": 23
    },
    {
      "_id": "5f3b9d40d9b75524dcc46587",
      "category_name": "Favourites",
      "category_no": 12,
      "status": 1,
      "selectable": 0,
      "order": 2
    },
    {
      "_id": "5f28e17b6a682e26f4377e75",
      "category_name": "Folktales",
      "category_no": 4,
      "status": 1,
      "order": 4,
      "selectable": 0
    },
    {
      "_id": "605189c77770a524f4a9a58e",
      "category_name": "Mystery",
      "category_no": 19,
      "status": 1,
      "selectable": 0,
      "order": 19
    },
    {
      "_id": "5f28e1eb6a682e26f4377e79",
      "category_name": "General Knowledge",
      "category_no": 8,
      "status": 1,
      "order": 8,
      "selectable": 0
    },
    {
      "_id": "5f28e12f8bbd8326f46ab7cf",
      "category_name": "Science-Fiction",
      "category_no": 2,
      "status": 1,
      "selectable": 0,
      "order": 12
    },
    {
      "_id": "61cbe44f9435dbf04a1c1d81",
      "category_name": "Action",
      "category_no": 23,
      "status": 1,
      "selectable": 0,
      "order": 23
    }
  ])

  const [viewall, setViewall] = React.useState(false)

  const [viewnumber, setViewnumber] = React.useState(5)

  const ChangeAction = (item, index) => {

    let ch = data.map((it, ind) => {
      var temp = Object.assign({}, it);
      if (ind === index) {
        temp.selectable = 1;
        return temp;
      } else {
        temp.selectable = 0;
        return temp;
      }
    });
    setdata(ch)
  };

  const viewallData = () => {
    if (viewnumber == 6) {
      setViewnumber(50)
    } else {
      setViewnumber(6)
    }
  }

  return (
    <View>
      <View style={{ flexDirection: "row", }}>
        <View style={{ flexDirection: 'row', flexWrap: "wrap", flex: 0.8, }}>
          {data.slice(0, viewnumber).map((item, index) => {
            return (
              <View style={{ marginRight: 10, }}>
                <TouchableOpacity
                  onPress={() => ChangeAction(item, index)}
                  style={{ borderWidth: 2, borderColor: "#46B2BE", borderRadius: 20, left: 10, marginTop: 10, paddingLeft: 10, paddingRight: 10, backgroundColor: item.selectable == 1 ? "#46b2be" : null }}>
                  <Text style={{ fontSize: 10, textAlign: "center", fontWeight: 'bold', color: item.selectable == 1 ? "#fff" : null }}> {item.category_name}</Text>
                </TouchableOpacity>
              </View>
            );
          })}


        </View>
        <View style={{ flex: 0.2, alignItems: "center", justifyContent: "center" }}>
          <TouchableOpacity style={{ alignSelf: "center", flexDirection: "row", }}>
            <Icon name="shuffle-on" size={15} color="#46b2be" style={{
              color: "#46b2be",
              alignSelf: "center",
              fontWeight: 'bold',
              right: 2
            }} />

            <Text style={styles.viewall}> shuffle</Text>
          </TouchableOpacity>
        </View>
      </View>
      <TouchableOpacity onPress={() => viewallData()}
        style={{ alignSelf: "center", flexDirection: "row", marginTop: 20 }}>
        <AIcon name="up" size={15} color="#46b2be" style={{

          color: "#46b2be",
          alignSelf: "center",
          fontWeight: 'bold',
          right: 2
        }} />

        <Text style={styles.viewall}>{viewnumber == 6 ? 'View all' : 'Close all'}  </Text>
      </TouchableOpacity>
    </View>
  )
};

const styles = StyleSheet.create({

  boxbedtime: {
    paddingTop: 3,
    height: 18,
    color: '#000',
    borderColor: "#46b2be",
    width: 90,
    bottom: 18,
    left: -5,
    borderRadius: 50,
    borderWidth: 1,
    fontSize: 10,
  },
  
  viewall: {
    fontSize: 10,
    color: "#46b2be",
    alignSelf: "center",
    fontWeight: 'bold',
  },


});

export default Flatlist;